package ru.t1.akolobov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum SortBy {

    BY_NAME("SortBy by name", "name"),
    BY_STATUS("SortBy by status", "status"),
    BY_CREATED("SortBy by created", "created");

    @NotNull
    private final String displayName;

    @NotNull
    private final String columnName;

    SortBy(@NotNull final String displayName, @NotNull final String columnName) {
        this.displayName = displayName;
        this.columnName = columnName;
    }

    @Nullable
    public static SortBy toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final SortBy sortBy : values()) {
            if (sortBy.name().equals(value)) return sortBy;
        }
        return null;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public String getColumnName() {
        return columnName;
    }

}
