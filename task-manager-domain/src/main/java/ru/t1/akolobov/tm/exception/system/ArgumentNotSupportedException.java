package ru.t1.akolobov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! This argument is not supported...");
    }

    public ArgumentNotSupportedException(@NotNull final String argument) {
        super("Error! Argument " + argument + " is not supported...");
    }

}
