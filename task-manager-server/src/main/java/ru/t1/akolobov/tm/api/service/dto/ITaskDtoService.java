package ru.t1.akolobov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.enumerated.Status;

import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDto> {

    @NotNull
    TaskDto changeStatusById(@Nullable String userId, @Nullable String id, Status status);

    @Nullable
    TaskDto create(@Nullable String userId, @Nullable String name);

    @Nullable
    TaskDto create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    TaskDto create(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    List<TaskDto> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    TaskDto updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}
