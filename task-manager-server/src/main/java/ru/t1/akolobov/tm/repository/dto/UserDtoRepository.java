package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.dto.model.UserDto;

import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserDtoRepository extends CommonDtoRepository<UserDto> {

    @NotNull
    Optional<UserDto> findByLogin(@NotNull String login);

    @NotNull
    Optional<UserDto> findByEmail(@NotNull String email);

    boolean existsByLogin(@NotNull String login);

    boolean existsByEmail(@NotNull String email);

}
