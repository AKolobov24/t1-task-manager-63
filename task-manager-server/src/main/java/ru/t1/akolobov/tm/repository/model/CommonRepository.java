package ru.t1.akolobov.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.akolobov.tm.model.AbstractModel;

@NoRepositoryBean
public interface CommonRepository<M extends AbstractModel> extends JpaRepository<M, String> {

}
