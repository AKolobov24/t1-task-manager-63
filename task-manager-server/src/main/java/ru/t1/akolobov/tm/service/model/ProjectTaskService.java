package ru.t1.akolobov.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.api.service.model.IProjectTaskService;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.akolobov.tm.exception.field.TaskIdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.repository.model.ProjectRepository;
import ru.t1.akolobov.tm.repository.model.TaskRepository;

@Service
@AllArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsByUserIdAndId(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = taskRepository.findByUserIdAndId(userId, taskId)
                .orElseThrow(TaskNotFoundException::new);
        @NotNull final Project project = projectRepository.findByUserIdAndId(userId, projectId)
                .orElseThrow(ProjectNotFoundException::new);
        task.setProject(project);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void unbindTaskFromProject(final @Nullable String userId, final @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final Task task = taskRepository.findByUserIdAndId(userId, taskId)
                .orElseThrow(TaskNotFoundException::new);
        task.setProject(null);
        taskRepository.save(task);
    }

}
