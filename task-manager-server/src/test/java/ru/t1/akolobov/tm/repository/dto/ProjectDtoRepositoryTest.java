package ru.t1.akolobov.tm.repository.dto;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.marker.UnitCategory;

import java.util.List;

import static ru.t1.akolobov.tm.data.dto.TestProjectDto.createProject;
import static ru.t1.akolobov.tm.data.dto.TestProjectDto.createProjectList;
import static ru.t1.akolobov.tm.data.dto.TestUserDto.*;

@Setter
@Category(UnitCategory.class)
public final class ProjectDtoRepositoryTest {

    @NotNull
    @Autowired
    private static UserDtoRepository userRepository;

    @NotNull
    @Autowired
    private static ProjectDtoRepository repository;


    @BeforeClass
    public static void prepareConnection() {
        userRepository.save(USER1);
        userRepository.save(USER2);
    }

    @AfterClass
    public static void closeConnection() {
        userRepository.delete(USER1);
        userRepository.delete(USER2);
    }

    @After
    public void deleteByUserIdData() {
        repository.deleteByUserId(USER1.getId());
        repository.deleteByUserId(USER2.getId());
    }

    @Test
    public void save() {
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.save(project);
        Assert.assertTrue(repository.findAllByUserId(USER1_ID).contains(project));
        Assert.assertEquals(1, repository.findAllByUserId(USER1_ID).size());
    }

    @Test
    public void deleteByUserId() {
        @NotNull final List<ProjectDto> projectList = createProjectList(USER1_ID);
        projectList.forEach(repository::save);
        long size = repository.count();
        repository.deleteByUserId(USER1_ID);
        Assert.assertEquals(
                size - projectList.size(),
                repository.count()
        );
    }

    @Test
    public void existsByUserIdAndId() {
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.save(project);
        Assert.assertTrue(repository.existsByUserIdAndId(USER1_ID, project.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(USER2_ID, project.getId()));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<ProjectDto> user1ProjectList = createProjectList(USER1_ID);
        @NotNull final List<ProjectDto> user2ProjectList = createProjectList(USER2_ID);
        user1ProjectList.forEach(repository::save);
        user2ProjectList.forEach(repository::save);
        Assert.assertEquals(user1ProjectList, repository.findAllByUserId(USER1_ID));
        Assert.assertEquals(user2ProjectList, repository.findAllByUserId(USER2_ID));
    }

    @Test
    public void findByUserIdAndId() {
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.save(project);
        @NotNull final String projectId = project.getId();
        Assert.assertEquals(project, repository.findByUserIdAndId(USER1_ID, projectId).orElse(null));
        Assert.assertNull(repository.findByUserIdAndId(USER2_ID, projectId).orElse(null));
    }

    @Test
    public void count() {
        @NotNull final List<ProjectDto> projectList = createProjectList(USER1_ID);
        projectList.forEach(repository::save);
        Assert.assertEquals(projectList.size(), repository.countByUserId(USER1_ID));
        repository.save(createProject(USER1_ID));
        Assert.assertEquals((projectList.size() + 1), repository.countByUserId(USER1_ID));
    }

    @Test
    public void delete() {
        createProjectList(USER1_ID).forEach(repository::save);
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.save(project);
        Assert.assertEquals(project, repository.findByUserIdAndId(USER1_ID, project.getId()).orElse(null));
        repository.delete(project);
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, project.getId()).orElse(null));
    }

    @Test
    public void deleteById() {
        createProjectList(USER1_ID).forEach(repository::save);
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.save(project);
        repository.deleteByUserIdAndId(USER1_ID, project.getId());
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, project.getId()).orElse(null));
    }

}