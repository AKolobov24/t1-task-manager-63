package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.dto.model.SessionDto;
import ru.t1.akolobov.tm.marker.UnitCategory;

import java.util.List;

import static ru.t1.akolobov.tm.data.dto.TestSessionDto.createSession;
import static ru.t1.akolobov.tm.data.dto.TestSessionDto.createSessionList;
import static ru.t1.akolobov.tm.data.dto.TestUserDto.*;

@Category(UnitCategory.class)
public final class SessionDtoRepositoryTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);
    @NotNull
    private final static UserDtoRepository userRepository = context.getBean(UserDtoRepository.class);

    @NotNull
    private final static SessionDtoRepository repository = context.getBean(SessionDtoRepository.class);

    @BeforeClass
    public static void prepareConnection() {
        userRepository.save(USER1);
        userRepository.save(USER2);
    }

    @AfterClass
    public static void closeConnection() {
        userRepository.delete(USER1);
        userRepository.delete(USER2);
    }

    @After
    public void deleteByUserIdData() {
        repository.deleteByUserId(USER1.getId());
        repository.deleteByUserId(USER2.getId());
    }

    @Test
    public void save() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final SessionDto session = createSession(USER1_ID);
        repository.save(session);
        Assert.assertEquals(session, repository.findAll().get(0));
        Assert.assertEquals(1, repository.findAll().size());
    }

    @Test
    public void deleteByUserId() {
        @NotNull final List<SessionDto> sessionList = createSessionList(USER1_ID);
        sessionList.forEach(repository::save);
        @NotNull final SessionDto user2Session = createSession(USER2_ID);
        repository.save(user2Session);
        Assert.assertEquals(sessionList.size() + 1, repository.findAll().size());
        repository.deleteByUserId(USER1_ID);
        Assert.assertEquals(1, repository.findAll().size());
        Assert.assertEquals(user2Session, repository.findAll().get(0));
    }

    @Test
    public void existById() {
        @NotNull final SessionDto session = createSession(USER1_ID);
        repository.save(session);
        Assert.assertTrue(repository.existsByUserIdAndId(USER1_ID, session.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(USER2_ID, session.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<SessionDto> user1SessionList = createSessionList(USER1_ID);
        @NotNull final List<SessionDto> user2SessionList = createSessionList(USER2_ID);
        user1SessionList.forEach(repository::save);
        user2SessionList.forEach(repository::save);
        Assert.assertEquals(user1SessionList, repository.findAllByUserId(USER1_ID));
        Assert.assertEquals(user2SessionList, repository.findAllByUserId(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull final SessionDto session = createSession(USER1_ID);
        repository.save(session);
        @NotNull final String sessionId = session.getId();
        Assert.assertEquals(session, repository.findByUserIdAndId(USER1_ID, sessionId).orElse(null));
        Assert.assertNull(repository.findByUserIdAndId(USER2_ID, sessionId).orElse(null));
    }

    @Test
    public void getSize() {
        @NotNull final List<SessionDto> sessionList = createSessionList(USER1_ID);
        sessionList.forEach(repository::save);
        Assert.assertEquals(sessionList.size(), repository.countByUserId(USER1_ID));
        repository.save(createSession(USER1_ID));
        Assert.assertEquals((sessionList.size() + 1), repository.countByUserId(USER1_ID));
    }

    @Test
    public void delete() {
        createSessionList(USER1_ID).forEach(repository::save);
        @NotNull final SessionDto session = createSession(USER1_ID);
        repository.save(session);
        Assert.assertEquals(session, repository.findByUserIdAndId(USER1_ID, session.getId()).orElse(null));
        repository.delete(session);
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, session.getId()).orElse(null));
    }

    @Test
    public void deleteById() {
        createSessionList(USER1_ID).forEach(repository::save);
        @NotNull final SessionDto session = createSession(USER1_ID);
        repository.save(session);
        repository.deleteByUserIdAndId(USER1_ID, session.getId());
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, session.getId()).orElse(null));
    }

}
