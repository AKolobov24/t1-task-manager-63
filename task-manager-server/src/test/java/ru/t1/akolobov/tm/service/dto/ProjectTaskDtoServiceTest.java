package ru.t1.akolobov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.akolobov.tm.exception.field.TaskIdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.akolobov.tm.repository.dto.TaskDtoRepository;
import ru.t1.akolobov.tm.repository.dto.UserDtoRepository;

import java.util.List;

import static ru.t1.akolobov.tm.data.dto.TestProjectDto.createProjectList;
import static ru.t1.akolobov.tm.data.dto.TestTaskDto.createTaskList;
import static ru.t1.akolobov.tm.data.dto.TestUserDto.*;

@Category(UnitCategory.class)
public class ProjectTaskDtoServiceTest {

    @Autowired
    private static IPropertyService propertyService;

    @Autowired
    private static UserDtoRepository userRepository;

    @Autowired
    private TaskDtoRepository taskRepository;

    @Autowired
    private ProjectDtoRepository projectRepository;

    @Autowired
    private IProjectTaskDtoService service;

    @BeforeClass
    public static void addUsers() {
        userRepository.save(USER1);
        userRepository.save(USER2);
    }

    @AfterClass
    public static void clearUsers() {
        userRepository.delete(USER1);
        userRepository.delete(USER2);
    }

    @Before
    public void initRepository() {
        createTaskList(USER1_ID).forEach(taskRepository::save);
        createProjectList(USER1_ID).forEach(projectRepository::save);
    }

    @After
    public void clearRepository() {
        taskRepository.deleteByUserId(USER1_ID);
        projectRepository.deleteByUserId(USER1_ID);
        taskRepository.deleteByUserId(USER2_ID);
        projectRepository.deleteByUserId(USER2_ID);
    }

    @Test
    public void bindTaskToProject() {
        @NotNull final ProjectDto project = projectRepository.findAllByUserId(USER1_ID).get(0);
        @NotNull final TaskDto task = taskRepository.findAllByUserId(USER1_ID).get(0);
        service.bindTaskToProject(USER1_ID, project.getId(), task.getId());
        final TaskDto bindTask = taskRepository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null);
        Assert.assertNotNull(bindTask);
        Assert.assertEquals(project.getId(), bindTask.getProjectId());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.bindTaskToProject(USER_EMPTY_ID, project.getId(), task.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> service.bindTaskToProject(USER1_ID, USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> service.bindTaskToProject(USER1_ID, project.getId(), USER_EMPTY_ID)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.bindTaskToProject(USER1_ID, project.getId(), project.getId())
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.bindTaskToProject(USER1_ID, task.getId(), project.getId())
        );
    }

    @Test
    public void removeProjectById() {
        @NotNull final ProjectDto project = projectRepository.findAllByUserId(USER1_ID).get(0);
        @NotNull final List<TaskDto> taskList = taskRepository.findAllByUserId(USER1_ID);
        service.bindTaskToProject(USER1_ID, project.getId(), taskList.get(0).getId());
        service.bindTaskToProject(USER1_ID, project.getId(), taskList.get(1).getId());

        service.removeProjectById(USER1_ID, project.getId());
        Assert.assertEquals(taskList.size() - 2, taskRepository.findAllByUserId(USER1_ID).size());
        Assert.assertFalse(taskRepository.findAllByUserId(USER1_ID).contains(taskList.get(0)));
        Assert.assertFalse(taskRepository.findAllByUserId(USER1_ID).contains(taskList.get(1)));

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.removeProjectById(USER_EMPTY_ID, project.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> service.removeProjectById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull final ProjectDto project = projectRepository.findAllByUserId(USER1_ID).get(0);
        @NotNull final TaskDto task = taskRepository.findAllByUserId(USER1_ID).get(0);
        service.bindTaskToProject(USER1_ID, project.getId(), task.getId());
        TaskDto bindTask = taskRepository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null);
        Assert.assertNotNull(bindTask);
        Assert.assertEquals(project.getId(), bindTask.getProjectId());

        service.unbindTaskFromProject(USER1_ID, task.getId());
        bindTask = taskRepository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null);
        Assert.assertNotNull(bindTask);
        Assert.assertNotEquals(project.getId(), bindTask.getProjectId());
        Assert.assertNull(bindTask.getProjectId());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.unbindTaskFromProject(USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> service.unbindTaskFromProject(USER1_ID, USER_EMPTY_ID)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.unbindTaskFromProject(USER1_ID, project.getId())
        );
    }

}
