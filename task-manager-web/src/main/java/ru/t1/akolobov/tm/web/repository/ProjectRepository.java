package ru.t1.akolobov.tm.web.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.web.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ProjectRepository {

    @NotNull
    private final static ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull
    private final Map<String, Project> projects = new LinkedHashMap<>();

    @NotNull
    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    {
        save(new Project("Alpha Project"));
        save(new Project("Betta Project"));
        save(new Project("Gamma Project"));
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void deleteById(@NotNull final String id) {
        projects.remove(id);
    }

}
