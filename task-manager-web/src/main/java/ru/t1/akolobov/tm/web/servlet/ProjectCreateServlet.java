package ru.t1.akolobov.tm.web.servlet;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.web.enumerated.Status;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/create/*")
public final class ProjectCreateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final Project project = new Project("New Project");
        ProjectRepository.getInstance().save(project);
        req.setAttribute("project", project);
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

}
