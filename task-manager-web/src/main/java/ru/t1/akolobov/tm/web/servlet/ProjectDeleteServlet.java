package ru.t1.akolobov.tm.web.servlet;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.web.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/delete/*")
public final class ProjectDeleteServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        if (id.isEmpty()) {
            resp.setStatus(406);
            return;
        }
        ProjectRepository.getInstance().deleteById(id);
        resp.sendRedirect("/projects");
    }

}
