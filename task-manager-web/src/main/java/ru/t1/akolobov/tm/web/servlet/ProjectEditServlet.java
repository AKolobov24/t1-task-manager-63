package ru.t1.akolobov.tm.web.servlet;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.web.enumerated.Status;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@WebServlet("/project/edit/*")
public final class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        if (id.isEmpty()) {
            resp.setStatus(406);
            return;
        }
        @Nullable final Project project = ProjectRepository.getInstance().findById(id);
        if (project == null) {
            resp.setStatus(404);
            return;
        }
        req.setAttribute("project", project);
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final Status status = Status.valueOf(req.getParameter("status"));
        @NotNull final String dateStartValue = req.getParameter("dateStart");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String dateFinishValue = req.getParameter("dateFinish");

        if (id.isEmpty() || name.isEmpty()) {
            resp.setStatus(406);
            return;
        }

        @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        @NotNull final Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setStatus(status);
        project.setDescription(description);
        try {
            project.setDateStart(dateStartValue.isEmpty() ? null : dateFormat.parse(dateStartValue));
            project.setDateFinish(dateFinishValue.isEmpty() ? null : dateFormat.parse(dateFinishValue));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }

}
