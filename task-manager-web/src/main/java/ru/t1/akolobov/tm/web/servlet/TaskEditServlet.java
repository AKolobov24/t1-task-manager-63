package ru.t1.akolobov.tm.web.servlet;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.web.enumerated.Status;
import ru.t1.akolobov.tm.web.model.Task;
import ru.t1.akolobov.tm.web.repository.ProjectRepository;
import ru.t1.akolobov.tm.web.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@WebServlet("/task/edit/*")
public final class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @Nullable final String id = req.getParameter("id");
        if (id == null || id.isEmpty()) {
            resp.setStatus(406);
            return;
        }
        @Nullable final Task task = TaskRepository.getInstance().findById(id);
        if (task == null) {
            resp.setStatus(404);
            return;
        }
        req.setAttribute("task", task);
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final Status status = Status.valueOf(req.getParameter("status"));
        @NotNull final String dateStartValue = req.getParameter("dateStart");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String dateFinishValue = req.getParameter("dateFinish");
        @NotNull final String projectId = req.getParameter("projectId");

        if (id.isEmpty() || name.isEmpty()) {
            resp.setStatus(406);
            return;
        }

        @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        @NotNull final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setStatus(status);
        task.setDescription(description);
        try {
            task.setDateStart(dateStartValue.isEmpty() ? null : dateFormat.parse(dateStartValue));
            task.setDateFinish(dateFinishValue.isEmpty() ? null : dateFormat.parse(dateFinishValue));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        task.setProjectId(projectId);

        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}
